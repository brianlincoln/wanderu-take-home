/*
    This config sets up basic js/css/json/assets support for webpack
    as well as a handful of helpful plugins. HtmlWebpackPlugin
    will build an index.html file and inject the necessary <link /> and
    <script /> tags to run your application. Feel free to change as
    you see fit
*/

const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const srcDirectory = path.join(__dirname, '../src');
const distDirectory = path.join(__dirname, '../dist/app');

const config = {
    bail: false,
    name: 'client',
    target: 'web',
    devtool: 'eval-source-map',
    context: srcDirectory,
    entry: {
        client: [
            'babel-polyfill',
            'react-hot-loader/patch',
            'webpack-dev-server/client?/',
            'webpack/hot/only-dev-server',
            './entries/client.jsx'
        ]
    },
    output: {
        path: distDirectory,
        filename: `static/js/[name].js`,
        chunkFilename: `static/js/[id].js`,
        publicPath: '/'
    },
    resolve: {
        modules: [
            srcDirectory,
            'node_modules'
        ],
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(js|jsx)$/,
                include: [srcDirectory],
                use: [
                    {
                        loader: 'eslint-loader',
                        options: {
                            configFile: '.eslintrc.js',
                            failOnError: false
                        }
                    }
                ]
            },
            {
                test: /\.(js|jsx)$/,
                include: [srcDirectory],
                use: [
                    'babel-loader'
                ],
            },
            {
                test: /\.css$/,
                include: [srcDirectory],
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            localIdentName: '[local].[hash:base64:5]',
                            modules: true,
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.json$/,
                include: [srcDirectory],
                loader: 'json-loader'
            },
            {
                test: /\.(ico|jpg|jpeg|png|gif|webp|svg)(\?.*)?$/,
                include: [srcDirectory],
                use: [{
                    loader: 'file-loader',
                    options: {
                        emitFile: true,
                        name: `static/media/[name].[ext]`
                    },
                }]
            },
            {
                test: /\.(eot|otf|ttf|woff|woff2)(\?.*)?$/,
                include: [srcDirectory],
                use: [{
                    loader: 'file-loader',
                    options: {
                        emitFile: true,
                        name: `static/fonts/[name].[ext]`
                    }
                }]
            },
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.NamedModulesPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../src/templates/app.ejs')
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        })
    ],
    node: {
        fs: 'empty'
    },
    devServer: {
        hot: true,
        historyApiFallback: true,
        inline: true,
        host: '0.0.0.0',
        proxy: {
            '/v2': {
                target: 'https://api.wanderu.com',
                secure: false,
                changeOrigin: true
            }
        },
        publicPath: '/'
    }
};

module.exports = config;
