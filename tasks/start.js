/*
    This task invokes webpack and starts the webpack dev server.
*/

const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ProgressBarWebpackPlugin = require('progress-bar-webpack-plugin');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const config = require('../config/webpack.config');


const compiler = webpack(config);
compiler.apply(new ProgressBarWebpackPlugin());
compiler.apply(new FriendlyErrorsWebpackPlugin());

new WebpackDevServer(compiler, config.devServer)
    .listen(3000, (err, result) => {
        if (err) {
            return console.log(err);
        }

        console.log('Listening at http://localhost:3000/');
    });