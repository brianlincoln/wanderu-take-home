# Brian's notes:

## Installation:
```
git clone https://bitbucket.org/brianlincoln/wanderu-take-home
cd wanderu-take-home
npm install
npm start
```

## Run Tests:
```
npm test
```

## Notes:
* I hadn't heard of "re-ducks" before, I really like it. Very neat file structure.

* I added some simple validation to the Search Page. In a larger project, I would abstract out form validation.

* Tim, sorry I bugged you with the API auth question. You pointed me in the right direction and I was able to get my own version of it working... Then as I re-read the README I saw that it explained, plain as day, that there is a provided apiService. So I switched to that, much cleaner and more robust than what I'd pieced together.

* Some trips didn't have pricing, in those cases I'm just showing "N/A"

* The carrier field looks more like carrier code/id's. Whereas the design has a nice name "Greyhound Lines", it is more like "GLI". I kept it, but I would investigate that if this were a real project.

* I pulled in Bootstrap, but it's really only used for the grid on the Results page. In a larger project I would either go all in with Bootstrap (or some other framework) or pull out just the parts I'm using. Curious to hear what you guys are doing on the CSS side.

* I added Jest, Enzyme and put in some basic tests. You can run them with "npm test". Not great coverage or anything, but a starting point. Similarly, interested in hearing what your standards are for front end testing.


# 
# 
# 
# OG README:
# 
# 
# TL;DR
* yarn (or npm) start - starts client-side spa

---

# Deliverable:

* Create a runnable client-side SPA where users can search for and view trips between two cities.

# Requirements:

* This application should consist of 2 pages:

    * A search page with two inputs, one for the origin location and one for the destination location, and a search button. Spec at https://zpl.io/brkNrkO

    * A results page that shows the 10 earliest trips between the user-submitted origin and destination for the next day. Spec at https://zpl.io/adA7MMp

* Make sure that the pages are "routable" - meaning that any page should be recreatable with only the url. A good test for this is reloading your page and ensuring the page is the same.

* This application should also leverage Redux to store application state/data to allow for decoupling of components and data.

# Search Page:

* The search page will live at the root of your route tree and have two inputs for origin location and destination location as well as a search button.

* The origin/destination location will be a text input that a user can freely type into.

* A json file at ducks/autocomplete/locations.json will contain an array of searchable locations a user should be able to search for on the search page. Each location is an object with three properties: `text`, which is the value that should be shown to users; `latLng`, an object representing a locations geographical latitude and longitude (the lat/lng will be used later to fetch trip between two lat/lng pairs); and `wcityid` which is a unique id for each location.

* As the user types, the application will search through the location data and show a dropdown of all matched locations. A user should be able to select a location from the dropdown to fill out the input field.

    * An InputDropdown component is available to you in the application skeleton. Feel free to use, modify, or ignore this component as you see fit.

* There should be a search button that when clicked will navigate the user to the search page. If an input is not filled, or both are filled with the same location, then the button should do nothing.

# Results Page:

* The results page should show the 10 earliest (or less if the api returns less than 10 trips) that are returned from the search.json endpoint.

* Each trip should show the carrier, depart time, depart station, return time, return station, and price, as well as a select button. (See https://zpl.io/adA7MMp).

# Bootstrapping:

* A basic webpack configuration will be provided to you. Outside of basic css, json, and asset support, the application will provide es6/es7 features as well as a webpack-dev-server with hot module replacement. To avoid CORs issues with the wanderu api, the dev server will proxy any request with /v2/* to wanderu's api for you. Feel free to change the webpack configuration to suit your needs.

* API documentation and helpers - you will be required to fetch resources from one of Wanderu's RESTful endpoints, described in `/apiDocumentation.txt`. In addition to this documentation, a basic api service will be provided to you that handles authentication and retries.

* An InputDropdown Component that manages a text input and an unordered list of options will be provided which might be useful for the search inputs.

* A `redux` folder (`/ducks`) contains basic reducer/action-creators/actions that can be used to "fetch" the relevant autocomplete options given a users input. One todo has been left if you choose to use this reducer.

# Additional considerations:

* Styling - Feel free to add any styling frameworks like Bootstrap to assist with the presentation.

* Accessibility - Accessibility is important to consider while developing your application. Not all users will have the same abilities and providing a great experience to everyone is a meaningful goal at Wanderu.

* SEO - while this application is client-side only (which greatly hinders SEO value), it is important to build pages that are easy for bots like google to parse and understand.



