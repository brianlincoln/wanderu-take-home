/*
    This file collects all reduces into an object to be combined into
    one reducer in src/reducers.js. You can add any additional reducers
    here
*/

import autocomplete from './autocomplete';
import trips from './trips';

const reducers = {
    autocomplete,
    trips
};

export default reducers