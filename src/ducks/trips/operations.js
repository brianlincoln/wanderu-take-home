import actions from './actions';
import locations from './../../locations.json';
import apiService from './../../services/apiService';

function fetchTrips(destination, origin) {
    return async (dispatch) => {
        dispatch(actions.fetchingTrips());

        const destinationObject = locations.find((location) => {
            return location.text === destination
        });    
        const originObject = locations.find((location) => {
            return location.text === origin
        });        
        const today = new Date();
        const tomorrow = new Date(today.setDate(today.getDate() + 1)).toISOString().split('T')[0];
        const apiPath = `/v2/search.json?origin_lat=${originObject.latLng.lat}&origin_lng=${originObject.latLng.lng}&dest_lat=${destinationObject.latLng.lat}&dest_lng=${destinationObject.latLng.lng}&depart_datetime=${tomorrow}`;

        return apiService.get(apiPath)
            .then((response) => {
                const results = response.result.sort((a, b) => {
                    return new Date(a.depart_datetime) - new Date(b.depart_datetime);
                }).slice(0, 10);

                dispatch(actions.setTrips(results));
            });
    }
}

export {    
    fetchTrips
};

export default {
    fetchTrips
};