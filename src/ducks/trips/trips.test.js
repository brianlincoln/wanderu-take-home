import actions from './actions';
import reducer from './reducers';

describe('trips duck', () => {
    it('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            fetching: false,
            data: []
        });
    });

    it('should handle fetchingTrips', () => {
        expect(reducer({}, actions.fetchingTrips())).toEqual({
            fetching: true
        });
    });
    
    it('should handle setTrips', () => {
        const trips = [
            {
                fakeData: "super fake"
            }
        ]

        expect(reducer({}, actions.setTrips(trips))).toEqual({
            fetching: false,
            data: trips
        });
    });
})