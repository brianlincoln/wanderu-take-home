import * as types from "./types";

function fetchingTrips() {
    return {
        type: types.FETCHING_TRIPS,
    };
};

function setTrips(data) {
    return {
        type: types.SET_TRIPS,
        payload: data,
    };
};

export {
    fetchingTrips,
    setTrips
};

export default {
    fetchingTrips,
    setTrips
};