import types from './types';

const INITIAL_STATE = {
    fetching: false,
    data: []
};

function _setTrips(state, action) {
    const {
        payload: data,
    } = action;

    return {
        ...state,
        fetching: false,
        data
    };
}

function _fetchingTrips(state) {
    return {
        ...state,
        fetching: true
    };
}

function reducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case types.SET_TRIPS:
            return _setTrips(state, action);
        case types.FETCHING_TRIPS:
            return _fetchingTrips(state);
        default:
            return state;
    }
}

export default reducer;
