#Ducks

At Wanderu we loosely follow the [re-ducks proposal](https://github.com/alexnm/re-ducks) for redux architecture.

Feel free to use this pattern or any other pattern you perfer, however the provided "autocomplete" redux loosely follows the re-ducks proposal.