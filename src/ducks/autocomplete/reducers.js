import types from './types';

const INITIAL_STATE = {
    fetching: false,
    data: []
};

function _setSuggestions(state, action) {
    const {
        payload: data,
    } = action;

    return {
        ...state,
        fetching: false,
        data
    };
}

function _fetchingSuggestions(state) {
    return {
        ...state,
        fetching: true
    };
}

function _clearSuggestions(state) {
    return {
        ...state,
        data: []
    };
}

function reducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case types.SET_SUGGESTIONS:
            return _setSuggestions(state, action);
        case types.FETCHING_SUGGESTIONS:
            return _fetchingSuggestions(state);
        case types.CLEAR_SUGGESTIONS:
            return _clearSuggestions(state);
        default:
            return state;
    }
}

export default reducer;
