import actions from './actions';
import locations from './../../locations.json';

function clearSuggestions() {
    return actions.clearSuggestions();
};

function fetchSuggestions(input) {
    return async (dispatch) => {
        dispatch(actions.fetchingSuggestions());

        // Keep abritrary delay to mimic async call
        return await new Promise((resolve) => {
            setTimeout(() => {
                const suggestions = locations.filter((suggestion) => {
                    return suggestion.text.toLowerCase().includes(input.toLowerCase());
                });

                dispatch(actions.setSuggestions(suggestions));
                resolve();
            }, 50);
        });
    }    
}

export {
    clearSuggestions,
    fetchSuggestions
};

export default {
    clearSuggestions,
    fetchSuggestions
};