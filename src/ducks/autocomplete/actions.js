import * as types from "./types";

function clearSuggestions() {
    return {
        type: types.CLEAR_SUGGESTIONS,
    };
};

function fetchingSuggestions() {
    return {
        type: types.FETCHING_SUGGESTIONS,
    };
};

function setSuggestions(data) {
    return {
        type: types.SET_SUGGESTIONS,
        payload: data,
    };
};

export {
    clearSuggestions,
    fetchingSuggestions,
    setSuggestions
};

export default {
    clearSuggestions,
    fetchingSuggestions,
    setSuggestions
};