/*
    This file sets up the redux store w/ middleware and redux
    devtools extension. Instructions for installing the extension
    can be found here: https://github.com/zalmoxisus/redux-devtools-extension
*/

import {
    createStore,
    applyMiddleware,
    compose
} from 'redux';

function configureStore(opts) {
    const {
        reducer,
        middleware,
        useDevToolExtension = false
    } = opts;

    let composeEnhancers = compose;
    if (useDevToolExtension) {
        if (!window.devToolsExtension) {
            console.warn('Application requested to use redux dev tools, but it is not installed.');
            return;
        }

        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }

    const store = createStore(
        reducer,
        {},
        composeEnhancers(
            applyMiddleware(...middleware)
        )
    );

    return store;
}

export default configureStore;
