/*
    This file sets up your "root reducer" and adds the thunk middleware.
    Feel free to add/replace any middleware you want
*/

import { combineReducers } from 'redux';
import thunk from 'redux-thunk';

import reducers from './ducks';

const middleware = [
    thunk
];

export {
    middleware,
    reducers
};

export default combineReducers(reducers);
