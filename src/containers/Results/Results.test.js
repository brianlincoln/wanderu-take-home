import React from 'react';
import { mount, shallow } from 'enzyme';

import { ResultsContainer } from './Results';

test('render a ResultsContainer that is fetching data (spinner)', () => {
    const props = {
        trips: {
            fetching: true,
        },
        
        fetchTrips: function() {}
    }
    const wrapper = mount (
        <ResultsContainer {...props} />
    );

    expect(wrapper.find('Spinner').exists());
});

test('render a ResultsContainer', () => {
    const props = {
        trips: {
            fetching: true,
        },
        
        fetchTrips: function() {}
    }

    const wrapper = shallow (
        <ResultsContainer {...props} />
    );

    expect(wrapper).toMatchSnapshot();
});