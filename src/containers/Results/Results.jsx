import React from 'react';
import { connect } from "react-redux";

import Trip from "./../../components/Trip/Trip"
import Spinner from "./../../components/Spinner/Spinner"
import { operations } from "../../ducks/trips";

import classNames from 'classnames';
import styles from './Results.css';

export class ResultsContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        
        this.props.fetchTrips(this.props.destination, this.props.origin);

        //SEO optimized page title. Normally, I would use react-helmet for title/meta tags
        document.title = `Wanderu - Bus & Train Deals from ${props.origin} to ${props.destination}`;
    }
    
    render() {
        if (this.props.trips.fetching) {
            return <Spinner />
        }

        const trips = this.props.trips.data.map((trip) => {
            return <Trip key={trip.trip_id} {...trip} />
        });

        return (
            <div className={classNames(styles.container)}>
                {trips}
            </div>            
        );
    }
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return({
        fetchTrips: (destination, origin) => dispatch(operations.fetchTrips(destination, origin)) 
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultsContainer);

