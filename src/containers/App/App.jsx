import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Search from '../../containers/Search/Search';
import Results from '../../containers/Results/Results';
import './App.css';

class AppContainer extends React.Component {
    render() {
        return (
          <Router>
              <Switch>
                <Route exact path="/" render={() => <Search history={this.props.history} />} />
                <Route path="/results/:origin/:destination" render={meta => (
                  <Results 
                    origin={meta.match.params.origin} 
                    destination={meta.match.params.destination}/>
                )} />
              </Switch>
          </Router>
        );
    }
};

export { AppContainer };
export default AppContainer;
