import React from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router";
import classNames from 'classnames';

import styles from './Search.css';
import { operations } from "../../ducks/autocomplete";
import InputDropdown from '../../components/InputDropdown/InputDropdown';

export class SearchContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            to: '',
            from: '',
            resultsPath: '',
            validationText: ''
        };

        //SEO optimized page title. Normally, I would use react-helmet for title/meta tags
        document.title = 'Search Bus & Train Deals - Wanderu';

        this._handleFromChange = this._handleFromChange.bind(this);
        this._handleToChange = this._handleToChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleFromChange(event) {
        this.setState({
            from: event.target.value,
            validationText: ''
        });
        operations.fetchSuggestions(event.target.value);
    }

    _handleToChange(event) {
        this.setState({
            to: event.target.value,
            validationText: ''
        });
        operations.fetchSuggestions(event.target.value);
    }

    _handleSubmit(event) {
        event.preventDefault();
        if (!this.state.from) {
            this.setState({validationText: "Please select an origin"});
        } else if (!this.state.to) {
            this.setState({validationText: "Please select a destination"});
        } else if (this.state.to === this.state.from) {
            this.setState({validationText: "Please select a different origin or destination"});
        } else {
            this.setState({resultsPath: encodeURI(`results/${this.state.from.text}/${this.state.to.text}`)}, () => {
                this.props.history.push(this.state.resultsPath);
            });            
        }
    }

    render() {
        return (
            <div className={classNames(styles.container)}>
                <form className={classNames(styles.searchForm)} onSubmit={this._handleSubmit} >                  
                    <InputDropdown 
                        options={this.props.autocomplete.data}
                        onBlur={this.props.clearSuggestions}
                        onChange={this._handleFromChange}
                        onInputChange={this.props.fetchSuggestions}
                        placeholder="From"
                        value={this.state.from !== undefined ? this.state.from.wcityid : null}
                        />
                    <InputDropdown 
                        options={this.props.autocomplete.data}
                        onBlur={this.props.clearSuggestions}
                        onChange={this._handleToChange}
                        onInputChange={this.props.fetchSuggestions}
                        placeholder="To"
                        value={this.state.from !== undefined ? this.state.from.wcityid : null}          
                        />
                    <button type='submit' ><i className="fas fa-search"></i></button>           
                </form>
                <div className={classNames(styles.validation)}>{this.state.validationText}</div>
          </div>
        );
    }
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return({
        clearSuggestions: () => dispatch(operations.clearSuggestions()),
        fetchSuggestions: (inputValue) => dispatch(operations.fetchSuggestions(inputValue)) 
    })
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchContainer));


