import React from 'react';
import { shallow } from 'enzyme';

import { SearchContainer } from './Search';

test('render a SearchContainer', () => {
    const props = {
        autocomplete: []
    }
    const wrapper = shallow (
        <SearchContainer {...props} />
    );

    expect(wrapper).toMatchSnapshot();
});