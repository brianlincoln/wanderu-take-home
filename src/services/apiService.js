/*
    This file contains a get and post function that will handle
    wanderu authentication and error mapping. You shouldn't need
    to modify this file, but feel free to if you want to.

    Note: Webpack will proxy '/v2/*' -> 'https://api.wanderu.com/v2/*'
    to avoid CORs issues.
*/

import fetch from 'isomorphic-fetch';

let _cachedAuthToken = undefined;
function assertResponseHasNoErrors(response) {
    if (!response.error || !response.error.codeKey) {
        return;
    }

    let error = new Error(response.error.codeKey);

    error.code = response.error.codeKey;
    error.responseBody = response;

    throw error;
}

function getHeaders(authToken = '') {
    const headers = {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
    };

    if (authToken) {
        headers['X-TOKEN'] = authToken;
    }

    return headers;
}

function isUnauthorized(response = {}) {
    return response.status === 401;
}

function hasAuthToken() {
    return !!_cachedAuthToken;
}

async function get(url, init = {}) {
    try {
        if (!hasAuthToken()) {
            await startNewSession();
        }

        const response = await fetch(url, {
            method: 'get',
            ...init,
            headers: {
                ...getHeaders(_cachedAuthToken),
                ...init.headers
            }
        });

        if (isUnauthorized(response)) {
            await startNewSession();
            return get(url, init);
        }

        const json = await response.json();

        assertResponseHasNoErrors(json);

        return json;
    } catch (ex) {
        console.error('get', url, init, ex);

        throw ex;
    }
}

async function post(url, init = {}) {
    try {
        if (!hasAuthToken()) {
            await startNewSession();
        }

        const response = await fetch(url, {
            method: 'post',
            ...init,
            headers: {
                ...getHeaders(_cachedAuthToken),
                ...init.headers
            },
            body: typeof init.body === 'string'
                ? init.body
                : JSON.stringify(init.body)
        });

        if (isUnauthorized(response)) {
            await startNewSession();
            return post(url, init);
        }

        const json = await response.json();

        assertResponseHasNoErrors(json);

        return json;
    } catch (ex) {
        console.error('post', url, init, ex);

        throw ex;
    }
}

async function startNewSession() {
    const payload = {
        credentials: {
            type: 'anonymous'
        }
    };

    try {
        const url = '/v2/auth.json';
        const response = await fetch(url, {
            method: 'post',
            body: JSON.stringify(payload),
            headers: getHeaders(_cachedAuthToken)
        });

        const json = await response.json();

        assertResponseHasNoErrors(json);

        _cachedAuthToken = json.result.token;

        return json.result;
    } catch (ex) {
        console.error('startNewSession', ex);
        throw ex;
    }
}

export default {
    get,
    post
};
