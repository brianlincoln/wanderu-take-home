/*
    This is the entry point of the application. It also sets up hot module
    reloading w/ the AppContainer plugin.
*/

import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

import configureStore from '../utils/configureStore';
import App from '../containers/App/App';
import rootReducer, {
    middleware as reduxMiddleware
} from '../reducer';

function hotRender(AppToRender, store) {
    render(
        <AppContainer>
            <BrowserRouter>
                <Provider store={store}>
                    <AppToRender />
                </Provider>
            </BrowserRouter>
        </AppContainer>,
        document.getElementById('root')
    );
}

function init() {
    const store = configureStore({
        middleware: reduxMiddleware,
        reducer: rootReducer,
        shouldUseDevTools: true
    });

    if (module.hot) {
        module.hot.accept('../containers/App/App', () => {
            const nextReducer = require('../containers/App/App.jsx').default;
            hotRender(nextReducer, store);
        });

        module.hot.accept('../reducer', () => {
            const nextReducer = require('../reducer').default;
            store.replaceReducer(nextReducer);
        });
    }

    hotRender(App, store);
}

init();
