import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Spinner.css';

function Spinner(props) {
    const { className } = props;

    return <div className={classNames(className, styles.spinner)}></div>
}

Spinner.propTypes = {
    className: PropTypes.string
}

Spinner.defaultProps = {
    className: '',
}

export default Spinner;