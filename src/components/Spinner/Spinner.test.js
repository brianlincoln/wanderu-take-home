import React from 'react';
import { shallow } from 'enzyme';

import Spinner from './Spinner';

test('render a Spinner', () => {
    const wrapper = shallow (
        <Spinner />
    );

    expect(wrapper).toMatchSnapshot();
});