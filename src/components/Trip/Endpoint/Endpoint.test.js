import React from 'react';
import { shallow } from 'enzyme';

import Endpoint from './Endpoint';

test('render an Endpoint', () => {
    const wrapper = shallow (
        <Endpoint
            cityName={'Minneapolis'}            
            endpointName={'Uptown Bus Station'}
            state={'MN'}
            time={'2018-07-10T23:30:00Z'} />
    );

    expect(wrapper).toMatchSnapshot();
});