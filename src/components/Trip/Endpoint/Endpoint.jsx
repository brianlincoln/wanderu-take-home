import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Endpoint.css';

//I borrowed the name "Endpoint" from your site's source code
function Endpoint(props) {
    const {
        cityName,
        className,
        endpointName,
        state,
        time } = props;

    //I would normally use moment.js for formatting dates/times
    function _formatAMPM(dateString) {
        const date = new Date(dateString);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        const ampm = hours >= 12 ? 'pm' : 'am';
        hours %= 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        return hours + ':' + minutes + ampm;    
    }
    
    return (
        <div className={classNames(className)}>
            <div className={classNames(styles.time)}>{_formatAMPM(time)}</div>
            <span className={classNames(styles.city)}>{cityName}, {state}</span>
            <div className={classNames(styles.endpointName)}>{endpointName}</div>
        </div>    
    );
}

Endpoint.propTypes = {
    cityName: PropTypes.string,
    className: PropTypes.string,
    endpointName: PropTypes.string,
    state: PropTypes.string,
    time: PropTypes.string
}

Endpoint.defaultProps = {
    cityName: '',
    className: '',
    endpointName: '',
    state: '',
    time: ''
}

export default Endpoint;
