import React from 'react';
import { shallow } from 'enzyme';

import Trip from './Trip';

test('render a Trip', () => {
    const wrapper = shallow (
        <Trip />
    );

    expect(wrapper).toMatchSnapshot();
});