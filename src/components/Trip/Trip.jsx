import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Endpoint from './Endpoint/Endpoint'

import styles from './Trip.css';

function Trip(props) {
    const {
        arrive_cityname,
        arrive_name,
        arrive_datetime,
        arrive_state,
        carrier,
        className,
        depart_cityname,
        depart_name,
        depart_datetime,
        depart_state,
        price } = props;

    function _formatPrice(rawPrice) {
        return rawPrice > 0 ? rawPrice.toLocaleString('en-US', {style: 'currency', currency: 'USD'}) : 'N/A';
    }

    return (
        <div className={classNames(className, styles.container, "row")} >     
            <Endpoint 
                className="col-xs-12 col-md-4 mb-2 mb-md-0"
                cityName={depart_cityname}
                endpointName={depart_name}
                time={depart_datetime}
                state={depart_state} />

            <Endpoint 
                className="col-xs-12 col-md-4 mb-2 mb-md-0"
                cityName={arrive_cityname}
                endpointName={arrive_name}
                time={arrive_datetime}
                state={arrive_state} />   

            <div className={classNames("col-xs-12 col-md-2 mb-2 mb-md-0")}>
                <div className={styles.price}>{_formatPrice(price)}</div>
                <div className={styles.carrier}>{carrier}</div>
            </div>

            <button className={classNames(styles.selectButton, "col-8 offset-2 col-sm-4 offset-sm-4 col-md-2 offset-md-0")}>Select</button>
        </div>

    );
}

Trip.propTypes = {
    arrive_cityname: PropTypes.string,
    arrive_name: PropTypes.string,
    arrive_datetime: PropTypes.string,
    arrive_state: PropTypes.string,
    carrier: PropTypes.string,
    className: PropTypes.string,
    depart_cityname: PropTypes.string,
    depart_name: PropTypes.string,
    depart_datetime: PropTypes.string,
    depart_state: PropTypes.string,
    price: PropTypes.number
}

Trip.defaultProps = {
    arrive_cityname: '',
    arrive_name: '',
    arrive_datetime: '',
    arrive_state: '',
    carrier: '',
    className: '',
    depart_cityname: '',
    depart_name: '',
    depart_datetime: '',
    depart_state: '',
    price: 0
}

export default Trip;
