import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Options.css';

function Options(props) {
    const {
        className,
        match,
        options,
        onMouseEnter,
        onMouseLeave,
        position,
        showOptions
    } = props;

    function _makeSubstrings(text, match) {
        function matchSpan(text, i = 0) {
            return (
                <span
                    className={styles.match}
                    key={`match-${i}`}
                >
                    {text}
                </span>
            );
        }

        function textSpan(text, i = 0) {
            return (
                <span
                    key={i}
                >
                    {text}
                </span>
            );
        }

        if (!text) {
            return null;
        }

        if (!match || match.length === 0) {
            return textSpan(text);
        }

        match = match.toLowerCase();
        if (text.toLowerCase() === match) {
            return matchSpan(text);
        }

        let substrings = [];
        let num = 0;
        while (text.length > 0) {
            const matchIndex = text.toLowerCase().indexOf(match);

            if (matchIndex === -1) {
                substrings.push(textSpan(text.substring(0), num));
                break;
            }

            if (matchIndex !== 0 ) {
                const textSubstring = text.substring(0, matchIndex);
                substrings.push(textSpan(textSubstring, num));
            }

            const matchSubstring = text.substring(matchIndex, matchIndex + match.length)
            substrings.push(matchSpan(matchSubstring, num));
            text = text.substring(matchIndex + match.length);
            num += 1;
        }

        return substrings;
    }

    if (!showOptions) {
        return null;
    }

    function Option(option, pos) {
        function _onMouseEnter() {
            return onMouseEnter(pos);
        }

        return (
            <div
                key={`${option.text}-${pos}`}
                className={classNames(
                    styles.optionContainer,
                    { [styles.highlighted]: pos === position }
                )}
                onMouseEnter={_onMouseEnter}
                onMouseLeave={onMouseLeave}
            >
                <span className={styles.option}>
                    {_makeSubstrings(option.text, match)}
                </span>
            </div>
        );
    }

    return (
        <ul className={classNames(
            className,
            styles.dropdown,
            styles.showCaret,
            { [styles.greyCaret]: position === 0 }
        )}>
            {options.map(Option)}
        </ul>
    );
}

function optionsPropTypeChecker(props, propName, componentName) {
    const options = props[propName];
    const textKeyMessage = 'Options must be an array of objects with a `text` key of type string.';
    if (!options || !Array.isArray(options)) {
        return new Error(
            `Required prop \`${propName}\` not supplied to \`${componentName}\`. ${textKeyMessage}`
        );
    }
    const allHaveTextKey = options.every(option => option.hasOwnProperty('text'))
    if (options.length > 0 && !allHaveTextKey) {
        return new Error(
            `Invalid prop \`${propName}\` supplied to \`${componentName}\`. ${textKeyMessage}`
        );
    }
}

Options.propTypes = {
    className: PropTypes.string,
    match: PropTypes.string,
    options: optionsPropTypeChecker,
    onMouseEnter: PropTypes.func,
    onMouseLeave: PropTypes.func,
    position: PropTypes.number,
    showOptions: PropTypes.bool
};

Options.defaultProps = {
    className: ``,
    match: ``,
    options: [
        { text: undefined }
    ],
    onMouseEnter() {},
    onMouseLeave() {},
    position: 0,
    showOptions: false
};

export default Options;
