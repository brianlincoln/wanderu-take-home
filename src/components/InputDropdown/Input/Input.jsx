import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './Input.css';

class Input extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        id: PropTypes.string,
        onBlur: PropTypes.func,
        onChange: PropTypes.func,
        onFocus: PropTypes.func,
        onKeyDown: PropTypes.func,
        onKeyUp: PropTypes.func,
        placeholder: PropTypes.string,
        type: PropTypes.string,
        value: PropTypes.string
    };

    static defaultProps = {
        className: ``,
        onBlur() {},
        onChange() {},
        onFocus() {},
        onKeyDown() {},
        onKeyUp() {},
        placeholder: ``,
        value: undefined
    };

    constructor(props) {
        super(props);

        this.state = {
            isFocused: props.autoFocus,
            passwordAsPlaintext: false
        };
    }

    get _hasFocusedLabel() {
        return this.state.isFocused || this.props.value;
    }

    _onBlur = (e) => {
        this.setState(() => ({ isFocused: false }));
        this.props.onBlur(e);
    }

    _onFocus = (e) => {
        this.setState(() => ({ isFocused: true }));
        this.props.onFocus(e);
    }

    _onKeyDown = (e) => {
        const { key } = e;
        if (key === 'ArrowUp') {
            this._preventCursorFromJumpingToStartOfInput(e);
        }

        this.props.onKeyDown(e);
    }

    _preventCursorFromJumpingToStartOfInput(e) {
        return e.preventDefault();
    }

    render() {
        const {
            className,
            id,
            onChange,
            onKeyUp,
            placeholder,
            value
        } = this.props;

        const {
            isFocused
        } = this.state;

        return (
            <div
                className={classNames(
                    className,
                    styles.container,
                    { [styles.focusedBorder]: isFocused }
                )}
            >
                <label
                    className={classNames(
                    styles.label,
                    { [styles.focusedLabel]: this._hasFocusedLabel }
                )}
                    htmlFor={id}
                >
                    {placeholder}
                </label>
                <input
                    id={id}
                    onBlur={this._onBlur}
                    onChange={onChange}
                    onFocus={this._onFocus}
                    onKeyDown={this._onKeyDown}
                    onKeyUp={onKeyUp}
                    placeholder=""
                    type="text"
                    value={value}
                />
            </div>
        );
    }
}

export default Input;
