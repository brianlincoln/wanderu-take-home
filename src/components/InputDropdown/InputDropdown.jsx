import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Input from './Input/Input';
import Options from './Options/Options';

import styles from './InputDropdown.css';

const BEGINNING_POSITION = -1;
const DEFAULT_HIGHLIGHT_POSITION = 0;
class InputDropdown extends React.Component {

    static propTypes = {
        className: PropTypes.string,
        options: PropTypes.array,
        onBlur: PropTypes.func,
        onChange: PropTypes.func,
        onFocus: PropTypes.func,
        onInputChange: PropTypes.func,
        placeholder: PropTypes.string,
        value: PropTypes.string
    };

    static defaultProps = {
        className: ``,
        options: [],
        onBlur() {},
        onChange() {},
        onFocus() {},
        onInputChange() {},
        placeholder: '',
        value: ``
    };

    constructor(props) {
        super(props);

        this.state = {
            isFocused: false,
            optionPosition: BEGINNING_POSITION,
            value: this.props.value
        }
    }

    //deprecated
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.state.value) {
            this.setState(() => ({
                value: nextProps.value
            }));
        }
    }

    get _value() {
        if (this._selectedOption) {
            return this._selectedOption.text;
        }

        return '';
    }

    get _selectedOption() {
        const { optionPosition } = this.state;
        if (optionPosition === BEGINNING_POSITION) {
            return undefined;
        }

        return this.props.options[optionPosition];
    }

    get _showOptions() {
        return this.state.isFocused && this.props.options.length !== 0;
    }

    _onInputFocus = (e) => {
        this.setState(() => ({
            isFocused: true
        }));

        this.props.onFocus(e);
    };

    _onInputBlur = e => {
        this.setState(
            () => ({
                isFocused: false,
                value: this._value,
            }),
            () => this.props.onBlur(e)
        );

        this.props.onChange({
            target: {
                value: this._selectedOption
            }
        });
    };

    _onInputChange = (e) => {
        const value = e.target.value;
        this.props.onInputChange(value);

        this.setState(() => ({ value }));

        if (!value) {
            return this.setState(() => ({
                optionPosition: BEGINNING_POSITION
            }));
        }

        const { optionPosition } = this.state;
        if (optionPosition === BEGINNING_POSITION) {
            return this.setState(() => ({
                optionPosition: DEFAULT_HIGHLIGHT_POSITION
            }));
        }
    };

    _onInputKeyDown = (e) => {
        const { key } = e
        if (key === 'Enter') {
            return this._blurFocusedInput();
        }

        if (key === 'Backspace') {
            this.setState(() => ({
                optionPosition: BEGINNING_POSITION
            }));
        }
    }

    _blurFocusedInput() {
        document.activeElement.blur();
    }

    _onInputKeyUp = (e) => {
        const { key } = e;
        if (!['ArrowUp', 'ArrowDown'].includes(key)) {
            return;
        }

        let { optionPosition } = this.state;
        if (key === 'ArrowUp') {
            optionPosition -= 1;
        }

        if (key === 'ArrowDown') {
            optionPosition += 1;
        }

        optionPosition = this._wrapOptionPosition(optionPosition);
        this.setState(() => ({ optionPosition }));
    };

    _onOptionsMouseEnter = (optionPosition) => {
        this.setState(() => ({ optionPosition }));
    };

    _onOptionsMouseLeave = (e) => {
        this.setState(() => ({
            optionPosition: BEGINNING_POSITION
        }));
    }

    _wrapOptionPosition(currentPosition) {
        const { options } = this.props;
        if (currentPosition < BEGINNING_POSITION) {
            return options.length - 1
        }

        if (currentPosition === options.length) {
            return BEGINNING_POSITION;
        }

        return currentPosition;
    }

    render() {
        const {
            className,
            options,
            placeholder
        } = this.props;
        const {
            isFocused,
            optionPosition,
            value
        } = this.state;

        return (
            <div className={classNames(
                className,
                styles.container
            )}>
                <Input
                    onBlur={this._onInputBlur}
                    onChange={this._onInputChange}
                    onFocus={this._onInputFocus}
                    onKeyDown={this._onInputKeyDown}
                    onKeyUp={this._onInputKeyUp}
                    placeholder={placeholder}
                    value={value}
                />
                <Options
                    match={value}
                    onMouseEnter={this._onOptionsMouseEnter}
                    onMouseLeave={this._onOptionsMouseLeave}
                    options={options}
                    position={optionPosition}
                    showOptions={isFocused && options.length > 0}
                />
            </div>
        );
    }
}

export default InputDropdown;